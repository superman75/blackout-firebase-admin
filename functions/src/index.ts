import * as functions from 'firebase-functions';
const admin = require('firebase-admin');
// const serviceAccount = require("../serviceAccountKey.json");
admin.initializeApp();
const env = functions.config();

const algoliasearch = require('algoliasearch');

//Initialize the Algolia client
const client = algoliasearch(env.algolia.appid, env.algolia.apikey);



/**
 * add manufactures collections from firestore to angolia indices
 */
exports.addFirestoreDataToAlgoliaManufactures = functions.https.onRequest((request, response) => {
	const records : any = [];
	const index = client.initIndex('manufactures');
	
	admin.firestore().collection('manufactures').get().then((docs:any) => {
		docs.forEach((doc:any) => {
			const manufacture = doc.data();
			manufacture.objectID = doc.id;
			manufacture.updated_at_milisecond = manufacture.updated_at._seconds*1000 + manufacture.updated_at._nanoseconds/1e6;
			manufacture.updated_at = new Date(manufacture.updated_at_milisecond);

			records.push(manufacture);
		});


		// Add or update new objects
		index
			.saveObjects(records)
			.then(() => {
				response.status(200).send('Contacts imported into Algolia from manufactures');
				console.log('Contacts imported into Algolia from manufactures');
			})
			.catch((error:any) => {
				console.error('Error when importing contact into Algolia from manufactures', error);
				response.status(404).send('Error when importing contact into Algolia from manufactures');
			});
		})
})

/**
* listen for modifying of every manufactures
*/
exports.modifyManufacture = functions.firestore
    .document('manufactures/{manufactureId}')
    .onWrite((change:any, context:any) => {
   
		const index = client.initIndex('manufactures');

		// Get an object with the current document value.
		// If the document does not exist, it has been deleted.
		const newDocument = change.after.exists ? change.after.data() : null;

		if(!newDocument){

			// Get Algolia's objectID from the Firebase object key
			const objectID = context.params.manufactureId;
			// Add or update object
			return index
				.deleteObject(objectID)
				.then(() => {
					console.log('Firebase object deleted from Algolia for manufactures', objectID);
				})
				.catch((error:any) => {
					console.error('Error when deleting contact from Algolia for manufactures', error);
					process.exit(1);
				});
		} else {

			// Get Firebase object
			const record = newDocument;
			// Specify Algolia's objectID using the Firebase object key
			record.objectID = context.params.manufactureId;
			record.updated_at_milisecond = record.updated_at._seconds*1000 + record.updated_at._nanoseconds/1e6;
			// Add or update object
			return index
				.saveObject(record)
				.then(() => {
					console.log('Firebase object indexed in Algolia from manufactures', record.objectID);
				})
				.catch((error:any) => {
					console.error('Error when indexing contact into Algolia from manufactures', error);
					process.exit(1);
				});
		}

    });


/**
 * add fixtures collections from firestore to angolia indices
 */
exports.addFirestoreDataToAlgoliaFixtures = functions.https.onRequest(async(request, response) => {
	const records : any = [];
	const index = client.initIndex('fixtures');

	const activeRef = await admin.firestore().collection('fixtures').get();

	let doc:any;
	for (doc of activeRef.docs){

		const fixture = doc.data();
		fixture.objectID = doc.id;
		fixture.updated_at_milisecond = fixture.updated_at._seconds*1000 + fixture.updated_at._nanoseconds/1e6;
		fixture.parent_manufacture = "";
		fixture.parent_folder = "";
		if(doc.data().manufacture_id){
			const doc1 = await doc.data().manufacture_id.get();
			fixture.parent_manufacture = doc1.data()?doc1.data().name:"";
		}
		if(doc.data().folder_id){
			const doc1 = await doc.data().folder_id.get();
			fixture.parent_folder = doc1.data()?doc1.data().name:"";
		}
		
		records.push(fixture);
	}

	// Add or update new objects
	index
		.saveObjects(records)
		.then(() => {
			response.status(200).send('Contacts imported into Algolia from fixtures');
			console.log('Contacts imported into Algolia from fixtures');
		})
		.catch((error:any) => {
			console.error('Error when importing contact into Algolia from fixtures', error);
			response.status(404).send('Error when importing contact into Algolia from fixtures');
		});
})

/**
* listen for modifying of every fixtures
*/
exports.modifyFixture = functions.firestore
    .document('fixtures/{fixtureId}')
    .onWrite(async(change:any, context:any) => {
   
		const index = client.initIndex('fixtures');

		// Get an object with the current document value.
		// If the document does not exist, it has been deleted.
		const newDocument = change.after.exists ? change.after.data() : null;

		if(!newDocument){

			// Get Algolia's objectID from the Firebase object key
			const objectID = context.params.fixtureId;
			// Add or update object
			return index
				.deleteObject(objectID)
				.then(() => {
					console.log('Firebase object deleted from Algolia for fixtures', objectID);
				})
				.catch((error:any) => {
					console.error('Error when deleting contact from Algolia for fixtures', error);
					process.exit(1);
				});
		} else {

			// Get Firebase object
			const record = newDocument;
			// Specify Algolia's objectID using the Firebase object key
			record.objectID = context.params.fixtureId;
			record.parent_manufacture = "";
			record.parent_folder = "";
			if(newDocument.manufacture_id){
				const doc = await newDocument.manufacture_id.get();
				record.parent_manufacture = doc.data()?doc.data().name:"";	
			}
			if(newDocument.folder_id){
				const doc = await newDocument.folder_id.get();
				record.parent_folder = doc.data()?doc.data().name:"";
			}
			record.updated_at_milisecond = record.updated_at._seconds*1000 + record.updated_at._nanoseconds/1e6;
			// Add or update object
			return index
				.saveObject(record)
				.then(() => {
					console.log('Firebase object indexed in Algolia for fixtures', record.objectID);
				})
				.catch((error:any) => {
					console.error('Error when indexing contact into Algolia for fixtures', error);
					process.exit(1);
				});
		}

    });


/**
 * add fixture_folders collections from firestore to angolia indices
 */
exports.addFirestoreDataToAlgoliaFixtureFolders = functions.https.onRequest((request, response) => {
	const records : any = [];
	const index = client.initIndex('fixture_folders');
	
	admin.firestore().collection('fixture_folders').get().then((docs:any) => {
		docs.forEach((doc:any) => {
			const fixture_folder = doc.data();
			fixture_folder.objectID = doc.id;
			fixture_folder.updated_at_milisecond = fixture_folder.updated_at._seconds*1000 + fixture_folder.updated_at._nanoseconds/1e6

			records.push(fixture_folder);
		});


		// Add or update new objects
		index
			.saveObjects(records)
			.then(() => {
				response.status(200).send('Contacts imported into Algolia from fixture_folders');
				console.log('Contacts imported into Algolia from fixture_folders');
			})
			.catch((error:any) => {
				console.error('Error when importing contact into Algolia from fixture_folders', error);
				response.status(404).send('Error when importing contact into Algolia from fixture_folders');
			});
		})
})

/**
* listen for modifying of every fixture_folders
*/
exports.modifyFixtureFolder = functions.firestore
    .document('fixture_folders/{fixture_folderId}')
    .onWrite((change:any, context:any) => {
   
		const index = client.initIndex('fixture_folders');

		// Get an object with the current document value.
		// If the document does not exist, it has been deleted.
		const newDocument = change.after.exists ? change.after.data() : null;

		if(!newDocument){

			// Get Algolia's objectID from the Firebase object key
			const objectID = context.params.fixture_folderId;
			// Add or update object
			return index
				.deleteObject(objectID)
				.then(() => {
					console.log('Firebase object deleted from Algolia for fixture_folders', objectID);
				})
				.catch((error:any) => {
					console.error('Error when deleting contact from Algolia for fixture_folders', error);
					process.exit(1);
				});
		} else {

			// Get Firebase object
			const record = newDocument;
			// Specify Algolia's objectID using the Firebase object key
			record.objectID = context.params.fixture_folderId;
			record.updated_at_milisecond = record.updated_at._seconds*1000 + record.updated_at._nanoseconds/1e6;
			// Add or update object
			return index
				.saveObject(record)
				.then(() => {
					console.log('Firebase object indexed in Algolia for fixture_folders', record.objectID);
				})
				.catch((error:any) => {
					console.error('Error when indexing contact into Algolia for fixture_folders', error);
					process.exit(1);
				});
		}

    });


/**
 * add fixture_channels collections from firestore to angolia indices
 */
exports.addFirestoreDataToAlgoliaFixtureChannels = functions.https.onRequest((request, response) => {
	const records : any = [];
	const index = client.initIndex('fixture_channels');
	
	admin.firestore().collection('fixture_channels').get().then((docs:any) => {
		docs.forEach((doc:any) => {
			const fixture_channel = doc.data();
			fixture_channel.objectID = doc.id;
			fixture_channel.updated_at_milisecond = fixture_channel.updated_at._seconds*1000 + fixture_channel.updated_at._nanoseconds/1e6;
			fixture_channel.snap = Number(fixture_channel.snap);
			fixture_channel.bit = Number(fixture_channel.bit);
			records.push(fixture_channel);
		});


		// Add or update new objects
		index
			.saveObjects(records)
			.then(() => {
				response.status(200).send('Contacts imported into Algolia from fixture_channels');
				console.log('Contacts imported into Algolia from fixture_channels');
			})
			.catch((error:any) => {
				console.error('Error when importing contact into Algolia from fixture_channels', error);
				response.status(404).send('Error when importing contact into Algolia from fixture_channels');
			});
		})
})

/**
* listen for modifying of every fixture_channels
*/
exports.modifyFixtureChannel = functions.firestore
    .document('fixture_channels/{fixture_channelId}')
    .onWrite((change:any, context:any) => {
   
		const index = client.initIndex('fixture_channels');

		// Get an object with the current document value.
		// If the document does not exist, it has been deleted.
		const newDocument = change.after.exists ? change.after.data() : null;

		if(!newDocument){

			// Get Algolia's objectID from the Firebase object key
			const objectID = context.params.fixture_channelId;
			// Add or update object
			return index
				.deleteObject(objectID)
				.then(() => {
					console.log('Firebase object deleted from Algolia for fixture_channels', objectID);
				})
				.catch((error:any) => {
					console.error('Error when deleting contact from Algolia for fixture_channels', error);
					process.exit(1);
				});
		} else {

			// Get Firebase object
			const record = newDocument;
			// Specify Algolia's objectID using the Firebase object key
			record.objectID = context.params.fixture_channelId;
			record.updated_at_milisecond = record.updated_at._seconds*1000 + record.updated_at._nanoseconds/1e6;
			record.snap = Number(record.snap);
			record.bit = Number(record.bit);
			// Add or update object
			return index
				.saveObject(record)
				.then(() => {
					console.log('Firebase object indexed in Algolia for fixture_channels', record.objectID);
				})
				.catch((error:any) => {
					console.error('Error when indexing contact into Algolia for fixture_channels', error);
					process.exit(1);
				});
		}

    });


/**
 * add channel_functions collections from firestore to angolia indices
 */
exports.addFirestoreDataToAlgoliaChannelFunctions = functions.https.onRequest(async(request, response) => {
	const records : any = [];
	const index = client.initIndex('channel_functions');
	let activeRef = await admin.firestore().collection('channel_functions').get();
	
	let doc:any;
	let iter = 0;
	for (doc of activeRef.docs){
		// if (doc.id<='ZfZG4bO2tTkHTPTvZjk7') continue;
		iter++;
		if(iter>600) break;
		const channel_function = doc.data();
		const doc1 = await doc.data().channel_id.get();
		channel_function.parent_channel_number = doc1.data()?doc1.data().number:null;
		channel_function.parent_fixture_name = ""
		if(doc1.data()){
			const doc2 = await doc1.data().fixture_id.get();
			channel_function.parent_fixture_name = doc2.data()?doc2.data().name:"";
		}
		
		channel_function.objectID = doc.id;
		channel_function.updated_at_milisecond = channel_function.updated_at._seconds*1000 + channel_function.updated_at._nanoseconds/1e6;
		console.log(doc.id);
		records.push(channel_function);
	}
	console.log("record length: " + records.length)

	// Add or update new objects
	index
		.saveObjects(records)
		.then(() => {
			response.status(200).send('Contacts imported into Algolia from channel_functions');
			console.log('Contacts imported into Algolia from channel_functions');
		})
		.catch((error:any) => {
			console.error('Error when importing contact into Algolia from channel_functions', error);
			response.status(404).send('Error when importing contact into Algolia from channel_functions');
		});
})

/**
* listen for modifying of every channel_functions
*/
exports.modifyChannelFunction = functions.firestore
    .document('channel_functions/{channel_functionId}')
    .onWrite(async(change:any, context:any) => {
   
		const index = client.initIndex('channel_functions');

		// Get an object with the current document value.
		// If the document does not exist, it has been deleted.
		const newDocument = change.after.exists ? change.after.data() : null;

		if(!newDocument){

			// Get Algolia's objectID from the Firebase object key
			const objectID = context.params.channel_functionId;
			// Add or update object
			return index
				.deleteObject(objectID)
				.then(() => {
					console.log('Firebase object deleted from Algolia for channel_functions', objectID);
				})
				.catch((error:any) => {
					console.error('Error when deleting contact from Algolia for channel_functions', error);
					process.exit(1);
				});
		} else {

			// Get Firebase object
			const record = newDocument;
			// Specify Algolia's objectID using the Firebase object key
			record.objectID = context.params.channel_functionId;
			record.updated_at_milisecond = record.updated_at._seconds*1000 + record.updated_at._nanoseconds/1e6;
			const doc = await newDocument.channel_id.get();
			record.parent_channel_number =  doc.data()?doc.data().number:null;
			record.parent_fixture_name = "";
			if(doc.data()){
				const doc1 = await doc.data().fixture_id.get();
				record.parent_fixture_name = doc1.data()?doc1.data().name:"";					
			}
			// Add or update object
			return index
				.saveObject(record)
				.then(() => {
					console.log('Firebase object indexed in Algolia for channel_functions', record.objectID);
				})
				.catch((error:any) => {
					console.error('Error when indexing contact into Algolia for channel_functions', error);
					process.exit(1);
				});
		}

    });


/**
 * add channel_conditions collections from firestore to angolia indices
 */
exports.addFirestoreDataToAlgoliaChannelConditions = functions.https.onRequest(async(request, response) => {
	const records : any = [];
	const index = client.initIndex('channel_conditions');
	let activeRef = await admin.firestore().collection('channel_conditions').get();
	let doc:any;

	for (doc of activeRef.docs){

		// if (doc.id<='ZxdTmTp1JhBPT7FkPsk0') continue;
		// console.log(doc.id)
		const channel_condition = doc.data();
		const doc1 = await channel_condition.dependent_channel_id.get();
		channel_condition.dependent_channel_number = doc1.data().number;
		const doc2 = await doc1.data().fixture_id.get();
		channel_condition.dependent_fixture_name = doc2.data()?doc2.data().name:"";
		channel_condition.objectID = doc.id;
		channel_condition.updated_at_milisecond = channel_condition.updated_at._seconds*1000 + channel_condition.updated_at._nanoseconds/1e6
		records.push(channel_condition);
	}
	console.log("record length: " + records.length)
	index
	.saveObjects(records)
	.then(() => {
		response.status(200).send('Contacts imported into Algolia from channel_conditions');
		console.log('Contacts imported into Algolia from channel_conditions');
	})
	.catch((error:any) => {
		console.error('Error when importing contact into Algolia from channel_conditions', error);
		response.status(404).send('Error when importing contact into Algolia from channel_conditions');
	});
		
})

/**
* listen for modifying of every channel_conditions
*/
exports.modifyChannelCondition = functions.firestore
    .document('channel_conditions/{channel_conditionId}')
    .onWrite(async(change:any, context:any) => {
   
		const index = client.initIndex('channel_conditions');

		// Get an object with the current document value.
		// If the document does not exist, it has been deleted.
		const newDocument = change.after.exists ? change.after.data() : null;

		if(!newDocument){

			// Get Algolia's objectID from the Firebase object key
			const objectID = context.params.channel_conditionId;
			// Add or update object
			return index
				.deleteObject(objectID)
				.then(() => {
					console.log('Firebase object deleted from Algolia for channel_conditions', objectID);
				})
				.catch((error:any) => {
					console.error('Error when deleting contact from Algolia for channel_conditions', error);
					process.exit(1);
				});
		} else {

			// Get Firebase object
			const record = newDocument;
			// Specify Algolia's objectID using the Firebase object key
			record.objectID = context.params.channel_conditionId;
			const doc = await newDocument.dependent_channel_id.get();
			record.dependent_channel_number = doc.data().number;
			const doc1 = await doc.data().fixture_id.get();
			record.dependent_fixture_name = doc1.data()?doc1.data().name:"";
			record.updated_at_milisecond = record.updated_at._seconds*1000 + record.updated_at._nanoseconds/1e6;
			// Add or update object
			return index
				.saveObject(record)
				.then(() => {
					console.log('Firebase object indexed in Algolia for channel_conditions', record.objectID);
				})
				.catch((error:any) => {
					console.error('Error when indexing contact into Algolia for channel_conditions', error);
					process.exit(1);
				});
		}

    });


exports.deleteRecordsMarkedDeleted = functions.https.onRequest(async(request, response) => {
	const collectioName = "fixture_channels";
	let activeRef = await admin.firestore().collection(collectioName).get();
	let doc:any;

	let count = 0;
	for (doc of activeRef.docs) {
		if(doc.data().deleted){
			admin.firestore().collection(collectioName).doc(doc.id).delete();
			count++;
		}
	}
	console.log('Deleted ' + count + ' records from ' + collectioName);
})
